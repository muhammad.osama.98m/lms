from flask import Flask, render_template, request, redirect, url_for
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime

app = Flask(__name__)
# /// = relative path, //// = absolute path
# app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+mysqlconnector://root:6621447@localhost:3306/test'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

class Readers(db.Model):
    uid = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(80))
    fname = db.Column(db.String(80))
    lname = db.Column(db.String(80))
    district = db.Column(db.String(80))
    hno = db.Column(db.Integer)
    stno = db.Column(db.Integer)

class Books(db.Model):
    isbn = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(80))
    category = db.Column(db.String(80))
    author = db.Column(db.String(80))
    uid = db.Column(db.Integer, db.ForeignKey('readers.uid'))

    Readers = db.relationship('Readers')

class Publisher(db.Model):
    pid = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))
    yop = db.Column(db.Date)
    isbn = db.Column(db.Integer, db.ForeignKey('books.isbn'))

    Books = db.relationship('Books')

class Reports(db.Model):
    rid = db.Column(db.Integer, primary_key=True)
    bid = db.Column(db.Integer)
    issue = db.Column(db.Date)
    retr = db.Column(db.Date, nullable=True)
    uid = db.Column(db.Integer, db.ForeignKey('readers.uid'))
    isbn = db.Column(db.Integer, db.ForeignKey('books.isbn'))

    Readers = db.relationship('Readers')
    Books = db.relationship('Books')

db.create_all()


@app.get("/")
def home():
    # report_rec = db.session.query(Reports).all()
    report_rec = db.session.execute('select * from Reports;')
    return render_template("base.html", report_rec=report_rec)


@app.post("/add")
def add():
    title = request.form.get("title").split(",")
    new_rec = Reports(rid=title[0], bid=title[1], issue=datetime.strptime(title[2], '%d/%m/%Y'), retr=datetime.strptime(title[3], '%d/%m/%Y'))
    db.session.add(new_rec)
    db.session.commit()
    return redirect(url_for("home"))


@app.get("/update/<int:rid>")
def update(rid):
    # rec = db.session.query(Reports).filter(Reports.rid == rid).first()
    # db.session.commit()
    return redirect(url_for("home"))


@app.get("/delete/<int:rid>")
def delete(rid):
    rec = db.session.query(Reports).filter(Reports.rid == rid).first()
    db.session.delete(rec)
    db.session.commit()
    return redirect(url_for("home"))


# Views for Books

@app.get("/books")
def home_2():
    books_rec = db.session.execute('select * from Books;')
    return render_template("2.html", books_rec=books_rec)


@app.post("/books/add")
def add_2():
    title = request.form.get("title").split(",")
    new_rec = Books(isbn=title[0], title=title[1], category=title[2], author=title[3])
    db.session.add(new_rec)
    db.session.commit()
    return redirect(url_for("home_2"))


@app.get("/books/update/<int:isbn>")
def update_2(isbn):
    # rec = db.session.query(Reports).filter(Reports.isbn == isbn).first()
    # db.session.commit()
    return redirect(url_for("home_2"))


@app.get("/books/delete/<int:isbn>")
def delete_2(isbn):
    rec = db.session.query(Books).filter(Books.isbn == isbn).first()
    db.session.delete(rec)
    db.session.commit()
    return redirect(url_for("home_2"))